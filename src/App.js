import React from 'react';
import './skin/index.sass';
import Ipfs from './ipfs';
import NoIpfs from './noIpfs';
import { homepage, bugs } from '../package.json';

function App() {
  return (
    <Ipfs noIpfs={ NoIpfs } links={ { homepage, issues: bugs.url } } >
    </Ipfs>
  );
}

export default App;
